![Capture2.JPG](https://bitbucket.org/repo/opyyy8/images/1855813783-Capture2.JPG)
![Capture3.JPG](https://bitbucket.org/repo/opyyy8/images/3830457896-Capture3.JPG)


### Sujet ###

* Workshop creative dev

### Développeur(s) ###
* Pauline Schnee

### COMMANDES INIT###
* npm install
* node server.js

url: 127.0.0.1:3000

### Principe ###
* Cercles avec symboles générés selon les lettres du prénom
* Partage : Envoie de son cercle sur interface avec tous les autres

### Technos / Libs ###

* Node / Socket IO

* GSAP
* canvas