/***********************
 * CIRCLE
 ***********************/
var Circle = function(context){
    this.context = context;
    this.userName = null;
    this.size = null;
};
Circle.prototype.createBasic = function () {

    //bg
   this.context.beginPath();
    this.context.fillStyle= App.bgColor;
    this.context.globalAlpha=1;
    this.context.arc(this.position.x,this.position.y,this.size/2, 0,2 * Math.PI,false);
    this.context.fill();
    this.context.closePath();

    //stroke
    this.context.beginPath();
    this.context.strokeStyle= App.mainColor;
    this.context.globalAlpha=1;
    this.context.lineWidth = 1;
    this.context.lineCap = "butt";
    this.context.setLineDash([0]);
    this.context.arc(this.position.x,this.position.y,this.size/2,0,2 * Math.PI,false);
    this.context.stroke();
    this.context.closePath();

};
Circle.prototype.createSymbols = function (letter) {
    this.context.beginPath();

    if(letter == "a"){
        this.context.fillStyle= '#e891bd';
        this.context.lineWidth = 5;
        this.context.globalAlpha = 1;
        if(App.theme=='light'){
            this.context.globalAlpha = 0.2;
        }
        this.context.moveTo(this.position.x, this.position.y-this.size/2);
        this.context.lineTo(this.position.x-this.size/2 * Math.cos(Math.PI/6), this.position.y+this.size/2 * Math.sin(Math.PI/6));
        this.context.lineTo(this.position.x+this.size/2 * Math.cos(Math.PI/6), this.position.y+this.size/2 * Math.sin(Math.PI/6));
        this.context.lineTo(this.position.x, this.position.y-this.size/2);
        this.context.lineJoin = 'bevel';
        this.context.fill();
    }
    if(letter == "b"){
        this.context.fillStyle= '#fde185';
        this.context.globalAlpha = 1;
        this.context.arc(this.position.x,this.position.y-this.size/4,this.size/10,0,2 * Math.PI,false);
        this.context.arc(this.position.x,this.position.y+this.size/4,this.size/10,0,2 * Math.PI,false);
        this.context.fill();
    }
    if(letter == "c" || letter == "ç"){
        this.context.strokeStyle= '#f69c9c';
        this.context.globalAlpha = 1;
        if(App.theme=='light'){
            this.context.globalAlpha = 0.4;
        }
        this.context.lineWidth = 5;
        this.context.setLineDash([this.size*3]);
        this.context.arc(this.position.x,this.position.y,this.size/2 + 10,0,2 * Math.PI,false);
        this.context.stroke();
    }
    if(letter == "d"){
        this.context.strokeStyle= '#fee182';
        this.context.globalAlpha = 1;
        if(App.theme=='light'){
            this.context.globalAlpha = 0.4;
        }
        this.context.lineWidth = 10;
        this.context.setLineDash([this.size/20]);
        this.context.arc(this.position.x,this.position.y,this.size/1.8,0,2 * Math.PI,false);
        this.context.stroke();
    }
    if(letter == "e" || letter == "é" || letter == "è" || letter == "ê"){
        this.context.strokeStyle= '#90cb99';
        this.context.globalAlpha = 1;
        this.context.lineWidth = 1;
        this.context.setLineDash([0]);
        this.context.moveTo(this.position.x-this.size/2 * Math.cos(Math.PI/4), this.position.y+this.size/2 * Math.sin(Math.PI/4));
        this.context.lineTo(this.position.x+this.size/2 * Math.cos(Math.PI/4), this.position.y-this.size/2 * Math.sin(Math.PI/4));
        this.context.moveTo(this.position.x-this.size/2 * Math.cos(Math.PI/6), this.position.y+this.size/2 * Math.sin(Math.PI/6));
        this.context.lineTo(this.position.x+this.size/2 * Math.cos(Math.PI/3), this.position.y-this.size/2 * Math.sin(Math.PI/3));
        this.context.moveTo(this.position.x-this.size/2 * Math.cos(Math.PI/3), this.position.y+this.size/2 * Math.sin(Math.PI/3));
        this.context.lineTo(this.position.x+this.size/2 * Math.cos(Math.PI/6), this.position.y-this.size/2 * Math.sin(Math.PI/6));
        this.context.stroke();
    }
    if(letter == "f"){
        this.context.strokeStyle= '#da87b9';
        this.context.globalAlpha = 1;
        this.context.lineWidth = 1;
        this.context.setLineDash([0]);

        this.context.moveTo(this.position.x-this.size/2 * Math.cos(Math.PI/5), this.position.y-this.size/2 * Math.sin(Math.PI/5));
        this.context.lineTo(this.position.x+this.size/2 * Math.cos(Math.PI/4), this.position.y+this.size/2 * Math.sin(Math.PI/4));
        this.context.moveTo(this.position.x-this.size/2 * Math.cos(Math.PI/4), this.position.y-this.size/2 * Math.sin(Math.PI/4));
        this.context.lineTo(this.position.x+this.size/2 * Math.cos(Math.PI/5), this.position.y+this.size/2 * Math.sin(Math.PI/5));
        this.context.stroke();
    }
    if(letter == "g"){
        this.context.lineWidth = 1;
        this.context.setLineDash([10]);
        this.context.strokeStyle = "#000000";
        this.context.globalAlpha = 1;
        this.context.moveTo(this.position.x, this.position.y);
        this.context.lineTo(this.position.x + this.size/2 + 5, this.position.y)
        this.context.stroke();
    }
    if(letter == "h"){
        this.context.globalAlpha=1;
        this.context.fillStyle= '#def5fb';
        if(App.theme=='light'){
            this.context.globalAlpha = 0.2;
        }
        this.context.rect(this.position.x-this.size/2 +10, this.position.y-this.size/2, 15, this.size);
        this.context.rect(this.position.x-this.size/2 +20 + 15, this.position.y-this.size/2, 15, this.size);
        this.context.fill();
    }
    if(letter == "i" || letter == "ï"){
        this.context.globalAlpha=1;
        this.context.fillStyle= '#fabc89';
        this.context.arc(this.position.x + this.size/2 * Math.cos(Math.PI/4), this.position.y - this.size/2 * Math.sin(Math.PI/4), 10, 0, 2 * Math.PI, false);
        this.context.fill();
    }
    if(letter == "j"){
        this.context.globalAlpha=1;
        this.context.fillStyle= '#fabc89';
        if(App.theme=='light'){
            this.context.globalAlpha = 0.3;
        }
        this.context.arc(this.position.x, this.position.y, this.size/2.5, 0, Math.PI, false);
        this.context.fill();
    }
    if(letter == "k"){
        this.context.globalAlpha=1;
        this.context.fillStyle= '#d5e185';
        if(App.theme=='light'){
            this.context.globalAlpha = 0.3;
        }
        this.context.arc(this.position.x, this.position.y, this.size/3, Math.PI+(Math.PI*3)/2, Math.PI+(Math.PI*2)/2, false);
        this.context.fill();
    }
    if(letter == "l"){
        this.context.globalAlpha=1;
        this.context.strokeStyle= '#82ccb5';
        this.context.lineWidth=1;
        this.context.setLineDash([0]);
        this.context.moveTo(this.position.x+this.size/2, this.position.y);
        this.context.lineTo(this.position.x+this.size/2 * Math.cos(Math.PI/4), this.position.y+this.size/2 * Math.sin(Math.PI/4));
        this.context.lineTo(this.position.x, this.position.y + this.size/2);
        this.context.lineTo(this.position.x-this.size/2 * Math.cos(Math.PI/4), this.position.y+this.size/2 * Math.sin(Math.PI/4));
        this.context.lineTo(this.position.x -this.size/2 , this.position.y);
        this.context.lineTo(this.position.x-this.size/2 * Math.cos(Math.PI/4), this.position.y-this.size/2 * Math.sin(Math.PI/4));
        this.context.lineTo(this.position.x, this.position.y - this.size/2);
        this.context.lineTo(this.position.x+this.size/2 * Math.cos(Math.PI/4), this.position.y-this.size/2 * Math.sin(Math.PI/4));
        this.context.lineTo(this.position.x + this.size/2, this.position.y);
        this.context.stroke();
    }
    if(letter == "m"){
        this.context.globalAlpha=1;
        this.context.strokeStyle= '#71abdd';
        this.context.strokeWidth= 1;
        this.context.setLineDash([0]);
        //bar1
        this.context.moveTo(this.position.x - this.size/2 - this.size*(1/6), this.position.y+10);
        this.context.lineTo(this.position.x - this.size/2, this.position.y-10);
        this.context.lineTo(this.position.x - (this.size/2) + this.size*(1/6), this.position.y+10);
        this.context.lineTo(this.position.x - (this.size/2) + this.size*(2/6), this.position.y-10);
        this.context.lineTo(this.position.x - (this.size/2) + this.size*(3/6), this.position.y+10);
        this.context.lineTo(this.position.x - (this.size/2) + this.size*(4/6), this.position.y-10);
        this.context.lineTo(this.position.x - (this.size/2) + this.size*(5/6), this.position.y+10);
        this.context.lineTo(this.position.x - (this.size/2) + this.size*(6/6), this.position.y-10);
        this.context.lineTo(this.position.x + this.size/2 + this.size*(1/6), this.position.y+10);
        this.context.stroke();

        //bar2
        this.context.setLineDash([10]);
        this.context.moveTo(this.position.x - this.size/2 - this.size*(1/6), this.position.y+5);
        this.context.lineTo(this.position.x - this.size/2, this.position.y-15);
        this.context.lineTo(this.position.x - (this.size/2) + this.size*(1/6), this.position.y+5);
        this.context.lineTo(this.position.x - (this.size/2) + this.size*(2/6), this.position.y-15);
        this.context.lineTo(this.position.x - (this.size/2) + this.size*(3/6), this.position.y+5);
        this.context.lineTo(this.position.x - (this.size/2) + this.size*(4/6), this.position.y-15);
        this.context.lineTo(this.position.x - (this.size/2) + this.size*(5/6), this.position.y+5);
        this.context.lineTo(this.position.x - (this.size/2) + this.size*(6/6), this.position.y-15);
        this.context.lineTo(this.position.x + this.size/2 + this.size*(1/6), this.position.y+5);
        this.context.stroke();
    }
    if(letter == "n"){
        this.context.globalAlpha=1;
        this.context.strokeStyle= '#ffed81';
        this.context.setLineDash([0]);
        this.context.lineWidth=3;

        this.context.moveTo(this.position.x - this.size/2, this.position.y+10);
        this.context.lineTo(this.position.x - (this.size/2) + this.size*(1/6), this.position.y-10);
        this.context.lineTo(this.position.x - (this.size/2) + this.size*(2/6), this.position.y+10);
        this.context.lineTo(this.position.x - (this.size/2) + this.size*(3/6), this.position.y-10);
        this.context.lineTo(this.position.x - (this.size/2) + this.size*(4/6), this.position.y+10);
        this.context.lineTo(this.position.x - (this.size/2) + this.size*(5/6), this.position.y-10);
        this.context.lineTo(this.position.x - (this.size/2) + this.size*(6/6), this.position.y+10);
        this.context.stroke();

        this.context.stroke();
    }
    if(letter == "o"){
        this.context.globalAlpha=1;
        this.context.fillStyle= '#70aadc';
        this.context.setLineDash([0]);
        this.context.arc(this.position.x, this.position.y, this.size/10, 0, Math.PI*2, false);
        this.context.fill();
    }
    if(letter == "p"){
        this.context.globalAlpha=1;
        this.context.fillStyle= '#f69c9b';
        if(App.theme=='light'){
            this.context.globalAlpha = 0.3;
        }
        this.context.setLineDash([0]);
        this.context.arc(this.position.x, this.position.y, this.size/2.5,  Math.PI*1.5, Math.PI*0.5, false);
        this.context.fill();
    }
    if(letter == "q"){
        this.context.lineWidth = 1;
        this.context.setLineDash([this.size/6]);
        this.context.strokeStyle = "#000000";
        this.context.globalAlpha = 1;
        this.context.moveTo(this.position.x, this.position.y);
        this.context.lineTo(this.position.x + this.size/2 *Math.cos(Math.PI/4), this.position.y+ this.size/2 *Math.cos(Math.PI/4));
        this.context.stroke();
    }
    if(letter == "r"){
        this.context.lineWidth = 1;
        this.context.setLineDash([0]);
        this.context.strokeStyle = "#8979b9";
        this.context.globalAlpha= 1;
        this.context.moveTo(this.position.x, this.position.y - this.size/2);
        this.context.lineTo(this.position.x +this.size/2, this.position.y);
        this.context.lineTo(this.position.x, this.position.y + this.size/2);
        this.context.stroke();

        this.context.setLineDash([3]);
        this.context.moveTo(this.position.x + 5, this.position.y - this.size/2);
        this.context.lineTo(this.position.x +this.size/2 +5, this.position.y);
        this.context.lineTo(this.position.x +5, this.position.y + this.size/2);
        this.context.stroke();
    }
    if(letter == "s"){
        this.context.lineWidth = 1;
        this.context.setLineDash([5]);
        this.context.strokeStyle = "#8979b9";
        this.context.globalAlpha= 1;
        this.context.moveTo(this.position.x, this.position.y);

        for (i = 0; i < 3*this.size; i++) {
            angle = 0.1 * i;
            x = this.position.x + (1 + 1 * angle) * Math.cos(angle);
            y = this.position.y + (1 + 1 * angle) * Math.sin(angle);

            this.context.lineTo(x, y);
        }
        this.context.stroke();
    }
    if(letter == "t"){
        this.context.lineWidth = 1;
        this.context.fillStyle = "#fecd7e";
        this.context.globalAlpha= 1;
        if(App.theme=='light'){
            this.context.globalAlpha = 0.3;
        }
        this.context.arc(this.position.x , this.position.y, this.size/2, 1.25*Math.PI, 1.75 *Math.PI, false);
        this.context.fill();
    }
    if(letter == "u"){
        this.context.lineWidth = 1;
        this.context.setLineDash([this.size/2]);
        this.context.strokeStyle = "#eeeb80";
        this.context.globalAlpha= 1;
        this.context.arc(this.position.x , this.position.y, this.size/1.85, Math.PI*2, Math.PI, false);
        this.context.stroke();
    }
    if(letter == "v"){
        this.context.fillStyle = "#70abdd";
        this.context.globalAlpha= 1;
        if(App.theme=='light'){
            this.context.globalAlpha = 0.3;
        }
        this.context.moveTo(this.position.x + this.size/2 * Math.cos(Math.PI/6), this.position.y + this.size/2 * Math.sin(Math.PI/6));
        this.context.lineTo(this.position.x, this.position.y + this.size/2);
        this.context.lineTo(this.position.x - this.size/2 * Math.cos(Math.PI/6), this.position.y + this.size/2 * Math.sin(Math.PI/6));
        this.context.fill();
    }
    if(letter == "w"){
        this.context.beginPath();
        this.context.fillStyle = "#82ccb5";
        this.context.globalAlpha= 1;
        if(App.theme=='light'){
            this.context.globalAlpha = 0.3;
        }
        this.context.moveTo(this.position.x + this.size/2 * Math.cos(Math.PI/6), this.position.y + this.size/2 * Math.sin(Math.PI/6));
        this.context.lineTo(this.position.x, this.position.y + this.size/2);
        this.context.lineTo(this.position.x - this.size/2 * Math.cos(Math.PI/6), this.position.y + this.size/2 * Math.sin(Math.PI/6));
        this.context.fill();
        this.context.closePath();

        this.context.beginPath();
        this.context.lineWidth = 1;
        this.context.setLineDash([10]);
        this.context.globalAlpha= 0.8;
        this.context.strokeStyle = "#82ccb5";
        this.context.moveTo(this.position.x + this.size/2 * Math.cos(Math.PI/3), this.position.y + this.size/2 * Math.sin(Math.PI/3));
        this.context.lineTo(this.position.x - this.size/2 * Math.cos(Math.PI/3), this.position.y + this.size/2 * Math.sin(Math.PI/3));
        this.context.lineTo(this.position.x, this.position.y + this.size/2 * Math.sin(Math.PI/6));
        this.context.lineTo(this.position.x + this.size/2 * Math.cos(Math.PI/3), this.position.y + this.size/2 * Math.sin(Math.PI/3));
        this.context.stroke();
        this.context.closePath();
    }
    if(letter == "x"){
        this.context.strokeStyle= '#da87b9';
        this.context.globalAlpha = 1;
        this.context.lineWidth = 2;
        this.context.setLineDash([0]);
        this.context.moveTo(this.position.x-this.size/2, this.position.y-this.size/2);
        this.context.lineTo(this.position.x-this.size/2 * Math.cos(Math.PI /4),this.position.y-this.size/2 * Math.sin(Math.PI /4));

        this.context.moveTo(this.position.x-this.size/2, this.position.y+this.size/2);
        this.context.lineTo(this.position.x-this.size/2 * Math.cos(Math.PI /4),this.position.y+this.size/2 * Math.sin(Math.PI /4));

        this.context.moveTo(this.position.x+this.size/2, this.position.y+this.size/2);
        this.context.lineTo(this.position.x+this.size/2 * Math.cos(Math.PI /4),this.position.y+this.size/2 * Math.sin(Math.PI /4));

        this.context.moveTo(this.position.x+this.size/2, this.position.y-this.size/2);
        this.context.lineTo(this.position.x+this.size/2 * Math.cos(Math.PI /4),this.position.y-this.size/2 * Math.sin(Math.PI /4));
        this.context.stroke();
    }
    if(letter == "y"){
        this.context.fillStyle= '#7b8ac6';
        this.context.globalAlpha = 1;
        if(App.theme=='light'){
            this.context.globalAlpha = 0.3;
        }

        this.context.arc(this.position.x , this.position.y, this.size/2, 1.25*Math.PI, 1.75 *Math.PI, false);

        this.context.moveTo(this.position.x-this.size/2 * Math.cos(Math.PI /4) , this.position.y-this.size/2 * Math.sin(Math.PI /4) );
        this.context.lineTo(this.position.x,this.position.y);
        this.context.lineTo(this.position.x+this.size/2 * Math.cos(Math.PI /4),this.position.y-this.size/2 * Math.sin(Math.PI /4) );

        this.context.fill();
    }
    if(letter == "z"){
        this.context.fillStyle= '#8ccda6';
        this.context.globalAlpha = 1;

        this.context.rect(this.position.x-this.size/2 * Math.cos(Math.PI /2.5) - Math.sqrt(200)/2.5, this.position.y-this.size/2 * Math.sin(Math.PI /2.5)  - Math.sqrt(200)/2.5, 10, 10);
        this.context.rect(this.position.x-this.size/2 * Math.cos(Math.PI /4) - Math.sqrt(200)/2.5, this.position.y-this.size/2 * Math.sin(Math.PI /4) - Math.sqrt(200)/2.5, 10, 10);
        this.context.rect(this.position.x-this.size/2 * Math.cos(Math.PI /10) - Math.sqrt(200)/2.5, this.position.y-this.size/2 * Math.sin(Math.PI /10) - Math.sqrt(200)/2.5, 10, 10);

        this.context.rect(this.position.x+this.size/2 * Math.cos(Math.PI /2.5) - Math.sqrt(200)/2.5, this.position.y+this.size/2 * Math.sin(Math.PI /2.5)  - Math.sqrt(200)/2.5, 10, 10);
        this.context.rect(this.position.x+this.size/2 * Math.cos(Math.PI /4) - Math.sqrt(200)/2.5, this.position.y+this.size/2 * Math.sin(Math.PI /4) - Math.sqrt(200)/2.5, 10, 10);
        this.context.rect(this.position.x+this.size/2 * Math.cos(Math.PI /10) - Math.sqrt(200)/2.5, this.position.y+this.size/2 * Math.sin(Math.PI /10) - Math.sqrt(200)/2.5, 10, 10);

        this.context.fill();
    }
    if(letter == "-"){
        this.context.strokeStyle= '#000000';
        this.context.globalAlpha = 1;
        this.context.lineWidth = 2;
        this.context.setLineDash([0]);
        this.context.moveTo(this.position.x-this.size/2 -10, this.position.y);
        this.context.lineTo(this.position.x-this.size/2, this.position.y);

        this.context.moveTo(this.position.x+this.size/2 +10, this.position.y);
        this.context.lineTo(this.position.x+this.size/2, this.position.y);

        this.context.stroke();
    }

    this.context.closePath();
};
Circle.prototype.build = function (position, size) {

    this.position= position;
    this.size = size;

    this.draw();
};
Circle.prototype.move = function (directionX, directionY, speed, posX, posY, depth) {

    this.directionX= directionX;
    this.directionY= directionY;
    this.speed= speed;
    this.targetX= posX;
    this.targetY= posY;
    this.depth= depth;

    this.draw();
};
Circle.prototype.draw = function () {
    this.createBasic();
    var i =0;
    if(this.userName != null){

        for(i; i<this.userName.length; i++){
            var symbol = this.userName[i].toLowerCase();
            this.createSymbols(symbol);
        }
    }
};





/***********************
 * WORLD
 ***********************/
var World = function(){
    this.canvas = null;
    this.context = null;

    this.particles = [];
};
World.prototype.init = function (el) {
    this.canvas = el;

    if(this.canvas && this.canvas.getContext) {
        this.context = this.canvas.getContext('2d');
        this.context.globalCompositeOperation = 'destination-over';
        window.addEventListener('resize', this.windowResizeHandler.bind(this), false);
        this.windowResizeHandler();
        this.loop();
    }

    document.addEventListener('mousemove', this.getMousePosition.bind(this));

};
World.prototype.initParticules = function (circles) {

    this.QUANTITY = circles.length;

    var i= 0;
    for (i; i < this.QUANTITY; i++) {
        var circle = circles[i];
        this.addParticle(circle);
    }
};
World.prototype.addParticle = function (circle) {

    //params
    var posX = circle.position.x;
    var posY = circle.position.y;
    var speed = 0.7;
    var directionX = -speed + (Math.random() * speed*2);
    var directionY = -speed + (Math.random()* speed*2);
    var depth = 0;
    var position= circle.position;
    var size = circle.size;

    //create Particule
    var particle = new Circle(this.context);
    particle.userName = circle.userName;
    particle.build({x:posX, y:posY}, size);
    particle.move(directionX, directionY, speed, posX, posY, depth);


    //add Particule
    this.particles.push( particle );

};
World.prototype.loop = function () {

    this.context.clearRect(0, 0, this.context.canvas.width, this.context.canvas.height);

    var z = 0;
    var xdist = 0;
    var ydist = 0;
    var dist = 0;

    for (var i=0; i < this.particles.length; i++){

        var particle = this.particles[i];


        var lp = { x: particle.position.x, y: particle.position.y };

        if(particle.position.x <=particle.size/2 || particle.position.x >= this.canvasWidth - particle.size/2){
            particle.directionX *= -1;
        }

        if(particle.position.y <=particle.size/2 || particle.position.y >= this.canvasHeight - particle.size/2){
            particle.directionY *= -1;
        }

        for(var s=0; s < this.particles.length; s++) {
            var bounceParticle = this.particles[s];
            if(bounceParticle.index != particle.index) {
                z = particle.size;
                xdist = Math.abs(bounceParticle.position.x - particle.position.x);
                ydist = Math.abs(bounceParticle.position.y - particle.position.y);
                dist = Math.sqrt(Math.pow(xdist, 2) + Math.pow(ydist, 2));
                if(dist < z) {
                    this.randomiseDirection(particle);
                    this.randomiseDirection(bounceParticle);
                }
            }
        }

        particle.position.x -= particle.directionX;
        particle.position.y -= particle.directionY;

        particle.draw();


        var particleLeft = particle.position.x - particle.size/2;
        var particleRight = particle.position.x + particle.size/2;
        var particleTop = particle.position.y - particle.size/2;
        var particleBottom = particle.position.y + particle.size/2;


        /*Affichage noms*/
        if(this.mouseX>particleLeft && this.mouseX<particleRight && this.mouseY>particleTop && this.mouseY<particleBottom){
            this.context.beginPath();
            this.context.fillStyle = App.mainColor;
            this.context.globalAlpha = 1;
            this.context.font = '40pt Lato';
            this.context.textAlign = 'center';
            this.context.fillText(particle.userName, particle.position.x, particle.position.y+15);
            this.context.closePath();
        }
    }

    requestAnimationFrame(this.loop.bind(this));

};
World.prototype.randomiseDirection = function (particle) {
    var d = 0;
    while((d == 0) || (d == 90) || (d == 180) || (d == 360)) {
        d = Math.floor(Math.random() * 360);
    }

    var r = (d * 180)/Math.PI;
    particle.directionX = Math.sin(r) * particle.speed;
    particle.directionY = Math.cos(r) * particle.speed;
};
World.prototype.windowResizeHandler = function() {
    this.canvasWidth = document.getElementById('circlesView').offsetWidth;
    this.canvasHeight = document.getElementById('circlesView').offsetHeight;
    this.canvas.width = this.canvasWidth;
    this.canvas.height = this.canvasHeight;
};
World.prototype.getMousePosition = function (e) {
    if(App.state == "world"){
        this.mouseX = e.clientX;
        this.mouseY = e.clientY;
    }
};






/***********************
 * circleGenerator
 ***********************/
var CircleGenerator = function(){
    this.canvas = null;
    this.input = null;
};
CircleGenerator.prototype.init = function (canvas, input) {
    this.input = input;
    this.canvas = canvas;
    this.default_circleSize = 100;
    this.section = document.getElementById('creation');

    this.initCanvas();
    this.initForm();
};
CircleGenerator.prototype.initForm = function () {
    this.last_nameLength = 0;
    this.input.value="";
    this.input.setAttribute('placeholder', '');

    this.input.focus();
    this.input.addEventListener("keyup", this.generateCircle.bind(this));
};
CircleGenerator.prototype.initCanvas = function () {

    if(this.canvas && this.canvas.getContext) {
        this.context = this.canvas.getContext('2d');
        this.context.globalCompositeOperation = 'destination-over';
        window.addEventListener('resize', this.windowResizeHandler.bind(this), false);
        this.windowResizeHandler();

        this.initCircle();
    }
    else{
        console.log("can't get canvas");
    }

};
CircleGenerator.prototype.initCircle = function () {
    var size = this.default_circleSize;
    var position = {
        x : (this.canvas.width / 2) - (size /2),
        y : (this.canvas.height / 2) - (size /2)
    };

    this.circle = new Circle(this.context);
    this.circle.build(position, size);

    this.endPercent = 101;
    this.curPerc = 0;
    this.animBasicsCircle();
};
CircleGenerator.prototype.generateCircle = function (e) {

    this.nameLength = this.input.value.length;
    this.circle.userName=this.input.value;

    /*update size*/
    if(this.nameLength>this.last_nameLength){
        this.circle.size += this.nameLength;

        if(App.audio.volume<0.9 && App.audio.volume>0){
            App.lastVolume+=0.1;
            App.audio.volume=App.lastVolume;
        }
        this.updateCircle();
    }else if(this.nameLength<this.last_nameLength){
        this.circle.size -= this.nameLength;
        if(App.audio.volume>0.1){
            App.lastVolume-=0.1;
            App.audio.volume=App.lastVolume;
        }
        this.updateCircle();
    }
    if(this.nameLength == 0){
        this.circle.size = this.default_circleSize;
    }
    if(this.nameLength >= 2){
        document.querySelector('#submit').style.opacity = 1;
    }


    this.last_nameLength = this.input.value.length;
};
CircleGenerator.prototype.updateCircle = function () {
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.circle.build(this.circle.position, this.circle.size, this.circle.basicColor);
    this.circle.draw();
};
CircleGenerator.prototype.windowResizeHandler = function() {
    this.canvasWidth = document.getElementById('circleView').offsetWidth;
    this.canvasHeight = document.getElementById('circleView').offsetHeight;
    this.canvas.width = this.canvasWidth;
    this.canvas.height = this.canvasHeight;
    this.input.setAttribute('placeholder', '');
    this.initCircle();
};

//animations
CircleGenerator.prototype.animBasicsCircle = function (current) {
    this.context.clearRect(0, 0, this.context.canvas.width, this.context.canvas.height);
    this.context.beginPath();
    this.context.strokeStyle = App.mainColor;
    this.context.arc(this.circle.position.x, this.circle.position.y, this.circle.size/2, -(Math.PI / 2), ((Math.PI * 2) * current) - (Math.PI / 2), false);
    this.context.stroke();
    this.curPerc++;
    if (this.curPerc < this.endPercent) {
        requestAnimationFrame(this.animBasicsCircle.bind(this, this.curPerc / 100))
    }
    if(this.curPerc == this.endPercent){
        this.animSectionIn();
    }

};
CircleGenerator.prototype.animSectionIn = function () {

    /*type placeholder*/
    var phText = "What's your name ?";
    this.phCount = 0;
    this.typePlaceholder(phText, this.input);

    /*elements animation*/
    var skip = document.querySelector('#skipCreate');
    var changeTheme = document.querySelector('#changeTheme');
    var sound = document.querySelector('#mute');

    var tl = new TimelineLite();
    tl.to(skip, 0.2, {opacity : 1} );
    tl.to(changeTheme, 0.2, {opacity : 1} );
    tl.to(sound, 0.2, {opacity : 1} );
    tl.add(function () {
        this.section.className='mainSection creationSection';
    }.bind(this), "+=2");


    App.worldSection.style.visibility = 'visible';
};
CircleGenerator.prototype.animSectionOut = function () {

    var tl = new TimelineLite();
    tl.add(function () {
        this.section.className+=' hide';
    }.bind(this));
    tl.to(this.section, 0, {display:'none'}, "+=1")

};
CircleGenerator.prototype.typePlaceholder = function (string, el) {

    var arr = [];
    var i =0;
    for(i; i<string.length; i++){
        arr.push(string[i]);
    }

    var input = el;
    var origString = string;

    // get current placeholder value
    var curPlace = input.getAttribute("placeholder");

    // append next letter to current placeholder
    var placeholder = curPlace + arr[this.phCount];


    // set timeout to use random speed to simulate human typing
    setTimeout(function(){

        input.setAttribute("placeholder", placeholder);
        this.phCount++;

        if (this.phCount < arr.length) {
            this.typePlaceholder(origString, input);
        }

    }.bind(this), Math.floor(Math.random() * (90-50+1)+50));

    /*requestAnimationFrame(function () {

        input.setAttribute("placeholder", placeholder);
        this.phCount++;

        if (this.phCount < arr.length) {
            this.typePlaceholder(origString, input);
        }
    }.bind(this))*/
};










/***********************
 * App
 ***********************/
var App = {

    io : io(),
    world : null,
    creator : null,
    state : null,
    theme : null,


    init: function(theme, state){

        this.state = state;
        this.theme = theme;

        document.addEventListener('DOMContentLoaded', this.loaded.bind(this));
        this.io.on('connect',this.connected.bind(this));
    },

    loaded : function(){

        /*toggle view*/
        var skipCreation = document.getElementById('skipCreate');
        var createAgain = document.getElementById('goCreate');
        skipCreation.addEventListener('click', this.toggleView.bind(this, 'world'));
        createAgain.addEventListener('click', this.toggleView.bind(this, 'creation'));

        /*toggle theme*/
        this.initTheme(this.theme);
        var changeTheme = document.getElementById('changeTheme');
        changeTheme.addEventListener('click', this.toggleTheme.bind(this));

        if(this.state == 'creation'){
            this.worldSection = document.getElementById('world');
            this.worldSection.style.visibility = 'hidden';

            /*intro opaque*/
            this.creationSection = document.getElementById('creation');
            this.creationSection.style += ' opaque';
        }

        /*music*/
        this.audio = document.getElementById('music');
        setTimeout(this.initSound.bind(this), 1000);


        this.initWorld();
        this.initCreation();
    },

    connected : function () {
        this.initCircles();

        var submitBtn = document.getElementById('submit');
        submitBtn.addEventListener("click", this.sendNewCircle.bind(this));


        document.addEventListener("keyup", function (e) {

            if(e.keyCode == 13 && this.state=='creation' && this.creator.nameLength>=2){
                this.sendNewCircle();
            }
        }.bind(this));


        this.getNewCircle();
    },

    initCreation : function () {
        this.creator = new CircleGenerator();
        var input = document.getElementById('pseudoInput');
        var canvas = document.getElementById('myCircle');

        this.creator.init(canvas, input);
    },

    initWorld : function () {
        var worldCanvas = document.getElementById('worldCanvas');
        this.world = new World();
        this.world.init(worldCanvas);
    },

    toggleView : function(state){
        console.log('change theme : ' + state);
        if(state == 'creation'){
            this.creationSection.style.display ='block';
            this.creationSection.className='mainSection creationSection';
            if(this.audio.volume > 0){
                this.lastVolume=0.1;
                this.audio.volume =  this.lastVolume;
            }
            this.initCreation();
            this.state='creation';
        }
        if(state == 'world'){
            this.creator.animSectionOut();
            if(this.audio.volume > 0){
                this.lastVolume=1;
                this.audio.volume =  this.lastVolume;
            }
            this.state='world';
        }
    },

    initTheme : function (theme) {
        this.theme = theme;
        if(this.theme == 'dark'){
            document.body.className = 'dark';
            this.bgColor = '#272727';
            this.mainColor = '#ffffff';
        }
        else if(this.theme == 'light'){
            document.body.className = 'light';
            this.bgColor = '#ffffff';
            this.mainColor = '#272727';
        }
    },

    toggleTheme : function(){
        if(this.theme == 'dark'){
           this.initTheme('light');
           this.initCreation();
        }
        else if(this.theme == 'light'){
            this.initTheme('dark');
            this.initCreation();
        }
    },

    initSound : function () {
        this.audio.play();
        this.lastVolume = 0.1;
        this.audio.volume = this.lastVolume;

        this.muteBtn = document.getElementById('mute');
        this.muteBtn.addEventListener('click', this.toggleSound.bind(this));
    },

    toggleSound : function () {

        if(this.audio.volume > 0){

            this.audio.volume = 0;
            this.muteBtn.className='isMutted';
        }
        else if(this.audio.volume == 0){

            this.audio.volume = this.lastVolume;
            this.muteBtn.className='';
        }
    },

    initCircles : function () {
        this.io.on('initCircles', function (circles) {
            this.world.initParticules(circles);
        }.bind(this));
    },

    sendNewCircle : function (e) {
        if(this.creator.circle.userName.length >= 2){
            this.toggleView('world');

            App.io.emit('sendNewCircle', this.creator.circle);
        }

    },

    getNewCircle : function () {
        this.io.on('getNewCircle', function (circle) {
            this.world.addParticle(circle);
        }.bind(this))
    }

};





/*requestAnimationFrame*/
(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); }, timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());

App.init('light', 'creation');
