/**
 * USER
 * @param id
 * @constructor
 */
var User = function (id) {
    this.id = id;
};






var App = {

    express : require('express')(),
    crypto : require('crypto'),
    path : require('path'),
    _ : require('lodash'),

    circles : [],


    init: function () {

        this.initSocket();
    },

    initSocket : function () {
        this.server = require('http').createServer(this.express);
        this.io = require('socket.io').listen(this.server);


        var express = require('express');
        this.express.use(express.static('static').bind(this));

        this.express.get('/', function (req, res) {
            res.sendFile(this.path.join(__dirname, 'templates', 'index.html'));
        }.bind(this));


        this.server.listen(3000, '127.0.0.1');

        this.io.on('connection', this.socketConnect.bind(this));
    },

    socketConnect : function (socket) {
        this.socket = socket;

        this.initUser();
        this.initCircles();
    },

    initUser : function () {

        var userId = this.crypto.randomBytes(20).toString('hex');
        this.user = new User(userId);

        this.socket.emit('getUser', this.user.id);
    },

    initCircles : function () {
        this.socket.emit('initCircles', this.circles);
        this.getCircle();
    },

    getCircle : function () {
        this.socket.on('sendNewCircle', function (circle) {

            this.circles.push(circle);

            this.getNewCircle(circle);

        }.bind(this));
    },

    getNewCircle : function (circle) {
        this.io.emit('getNewCircle', circle);
    }

};

App.init();